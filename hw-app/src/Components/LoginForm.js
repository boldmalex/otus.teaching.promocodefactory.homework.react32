import React,{useState} from "react";
import '../Styles/Auth.css'
import axios from "axios";


const LoginForm = () =>{

    const [userName, setUserName] = useState('');
    const [userPass, setUserPass] = useState('');
    const [isLogged, setLogged] = useState(false);
    const [errorMessage, setErrorMessage] = useState('')


    function apiLogin (userName, userPassword) {
        console.log("api login");
        axios.post('/api/v1/Login', {
                userName: userName,
                userPass: userPassword
            },
            {
                headers: {"content-type": "application/json"}
            }
        )
        .then(result => {
            console.log('api success')
            console.log(result);
            if (result.isAuthenticated){
                setLogged(true);
                setUserName(userName);
            }
            else{
                console.log('api success login Invalid');
                setErrorMessage('invalid login or pass');
            }
                
        })
        .catch(() =>{
            console.log('api error')
            setErrorMessage('Api send error');
        })
    }


    function handleSubmit(e){
        //e.preventDefault();
        console.log("handle submit");
        let userName = e.target.elements.UserName.value
        let userPassword = e.target.elements.UserPass.value
        apiLogin(userName, userPassword);
    }


    return(
        <div>
            <p style={{ color: 'red' }}>{errorMessage}</p>

            <form onSubmit={handleSubmit}>
                <input type='text' name="UserName" placeholder='Имя пользователя' required></input>
                <input type='password' name="UserPass" placeholder='Пароль' required></input>
                <input type='submit'></input>
            </form>
        </div>
    )
}

export default LoginForm;