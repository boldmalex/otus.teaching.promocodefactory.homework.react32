﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class UserLoginResponse
    {
        public string UserName { get; set; }
        public bool isAuthenticated { get; set; }
    }
}
