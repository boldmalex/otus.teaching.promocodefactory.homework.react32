﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class UserLoginRequest
    {
        [Required]
        [MinLength(1)]
        public string UserName { get; set; }
        [Required]
        [MinLength(3)]
        public string UserPass { get; set; }
    }
}
