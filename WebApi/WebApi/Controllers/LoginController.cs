﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Controllers
{

    [ApiController]
    [Route("api/v1/[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly List<User> fakeUsers = new List<User>()
        {
            new User(){ UserName = "Test", UserPass = "Test"},
            new User(){ UserName = "Test2", UserPass = "Test2"},
        };

        [HttpPost]
        public UserLoginResponse Login([FromBody]UserLoginRequest user)
        {
            var result = new UserLoginResponse()
            {
                isAuthenticated = false,
                UserName = ""
            };


            if (ModelState.IsValid)
            {
                if (fakeUsers.Any(x => x.UserName.Equals(user.UserName)
                                    && x.UserPass.Equals(user.UserPass)))
                {
                    result = new UserLoginResponse()
                    {
                        isAuthenticated = true,
                        UserName = user.UserName
                    };
                }
            }

            return result;
        }
    }
}
